#ifndef TEMPLATE__MATCH__HPP
#define TEMPLATE__MATCH__HPP

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <iterator>

class TemplatePoseMatch
{
	public:
	
		static std::vector<cv::Mat>::iterator Match(std::vector<cv::Mat>& template_views, const cv::Mat& scene)
		{
			float min_error(255*255*scene.total());
			cv::Mat curr_error(1,1,CV_32F);
			std::vector<cv::Mat>::iterator it = template_views.begin();
			std::vector<cv::Mat>::iterator best_view_it = it;
			for (auto v : template_views)
			{	cv::resize(v,v,cv::Size(),0.2,0.2);
				cv::matchTemplate(scene,v,curr_error,cv::TemplateMatchModes::TM_SQDIFF);
				if (curr_error.at<float>(0,0)<min_error)
				{	min_error = curr_error.at<float>(0,0);
					best_view_it = it;
				}
				it++;
			}
			return best_view_it;
		};
};

#endif //TEMPLATE__MATCH__HPP
