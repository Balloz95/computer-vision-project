#ifndef OBJECT__DETECTION__HPP
#define OBJECT__DETECTION__HPP

#include <opencv2/xfeatures2d.hpp>

class ObjectDetection
{
	private:
		
		//Member variables
		cv::Mat object_;
		std::vector<cv::KeyPoint> keypoints_;
		cv::Mat descriptors_;
		static const cv::Ptr<cv::xfeatures2d::SIFT> kSiftDetector;
		static const cv::Ptr<cv::BFMatcher> kMatcher;
				
		//Functions
		std::vector<cv::DMatch> FindAccurateMatches(const cv::Mat&, const cv::Mat&, const double&);
		
	public:
	
		//Constructors
		ObjectDetection(const cv::Mat&);
		
		//Methods
		bool DrawBoundingBox(const cv::Mat&, cv::Mat&);
		std::vector<cv::KeyPoint> get_keypoints(void);
		cv::Mat get_descriptors(void);
};

#endif //OBJECT__DETECTION__HPP
