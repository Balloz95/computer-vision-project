#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include "ObjectDetection.hpp"
#include "TemplatePoseMatch.hpp"

#include <iostream>

using namespace cv;
using namespace std;

int main(int argc,char** argv){
	
	vector<string> all_templates_dirs;
	vector<vector<Mat>> all_templates_views;
	vector<String> curr_templates_file_names;
	vector<Mat> curr_templates_views;
	Mat scene;
	string curr_templates_dir = "";
	string scene_filename = "";
	
	while (true)
	{	//User input reading
		
		while (true)
		{	cout<<"Enter templates directory's name:"<<endl;
			getline(cin,curr_templates_dir);
			try
			{	glob("../"+curr_templates_dir+"/*.png",curr_templates_file_names);
				while (true)
				{	cout<<"Enter scene file's name (without extension):"<<endl;
					getline(cin,scene_filename);
					try
					{	string scene_dir = scene_filename.substr(0,scene_filename.find_first_of("_"));
						scene = imread("../"+scene_dir+"/"+scene_filename+".png");
						if (!scene.empty())
							break;
						else
							cout<<"Invalid setting."<<endl;
					}
					catch (cv::Exception)
					{	cout<<"Invalid setting."<<endl;
					}
				}
				break;
			}
			catch (cv::Exception)
			{	cout<<"Invalid setting."<<endl;
			}
		}
		
		//Load templates dataset
	
		int num_views = curr_templates_file_names.size();
		curr_templates_views.resize(num_views);
		int curr_templates_dir_index = distance(all_templates_dirs.begin(),find(all_templates_dirs.begin(),all_templates_dirs.end(),curr_templates_dir));
		if (curr_templates_dir_index == all_templates_dirs.size())
		{	all_templates_dirs.resize(all_templates_dirs.size()+1);
			all_templates_dirs[all_templates_dirs.size()-1] = curr_templates_dir;
			cout<<"Loading templates images..."<<endl;
			for (int i=0; i<num_views; i++)
				curr_templates_views[i] = imread(curr_templates_file_names[i],IMREAD_GRAYSCALE);
			cout<<"All templates loaded."<<endl;
			all_templates_views.resize(all_templates_dirs.size());
			all_templates_views[all_templates_views.size()-1] = curr_templates_views;
		}
		else
			curr_templates_views = all_templates_views[curr_templates_dir_index];		
		
		//Pose estimation with template matching
		
		Mat gray_scene(scene.clone());
		cvtColor(gray_scene,gray_scene,CV_BGR2GRAY);
		resize(gray_scene,gray_scene,Size(),0.2,0.2);
		int best_view_index(distance(curr_templates_views.begin(),TemplatePoseMatch::Match(curr_templates_views,gray_scene)));
		
		//Check object detection
		
		String best_view = curr_templates_file_names[best_view_index];
		ObjectDetection template_to_detect(curr_templates_views[best_view_index]);
		if (!template_to_detect.ObjectDetection::DrawBoundingBox(scene,scene))
			cout<<"The given object is not present in the scene."<<endl;
		else
		{	//Show template and scene and print out estimated angles
			
			Mat object(imread(curr_templates_file_names[0]));
			Mat show_orientation(object.rows,object.cols+scene.cols,object.type());
			hconcat(object,scene,show_orientation);
			resize(show_orientation,show_orientation,Size(),0.7,0.7); //for visualization purpose
			imshow("Template found in scene",show_orientation);
			String rot_angle = best_view.substr(best_view.size()-7,3);
			String cur_angle = best_view.substr(best_view.size()-15,3);
			while (rot_angle[0] == '0' && rot_angle.size() > 1)
				rot_angle = rot_angle.substr(1);
			while (cur_angle[0] == '0' && cur_angle.size() > 1)
				cur_angle = cur_angle.substr(1);
			cout<<"The given object is present in the scene with a curve angle of about "+cur_angle+" degrees and a rotation angle of about "+rot_angle+" degrees."<<endl;
			waitKey(0);
			destroyAllWindows();
		}
		
		//Quit program or repeat angle estimation
		
		string quit = "";
		while (quit != "y" && quit != "n")
		{	cout<<"Quit? (y/n)"<<endl;
			getline(cin,quit);
		}
		if (quit == "y")
			break;
	}		
	return(0);
}
