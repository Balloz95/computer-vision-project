#include <opencv2/imgproc.hpp>
#include <opencv2/ccalib.hpp>
#include "ObjectDetection.hpp"

#include <numeric>

using namespace cv;
using namespace std;

const Ptr<BFMatcher> ObjectDetection::kMatcher(BFMatcher::create());	//static variables - not associated with any instace
const Ptr<xfeatures2d::SIFT> ObjectDetection::kSiftDetector(xfeatures2d::SIFT::create());

ObjectDetection::ObjectDetection(const Mat& object):

	object_(object)

{	kSiftDetector->detectAndCompute(object_,Mat(),keypoints_,descriptors_,false);	//detect keypoints and compute descriptors
};

vector<KeyPoint> ObjectDetection::get_keypoints()
{
	return keypoints_;
}
		
Mat ObjectDetection::get_descriptors()
{
	return descriptors_;
}

bool ObjectDetection::DrawBoundingBox(const Mat& scene, Mat& dst)
{
	ObjectDetection current_scene = ObjectDetection(scene);
	vector<KeyPoint> scene_keypoints(current_scene.keypoints_);
	Mat scene_descriptors(current_scene.descriptors_);
	vector<DMatch> matches(FindAccurateMatches(scene_descriptors, descriptors_,3));	//ratio set with trial-and-error
	size_t num_matches(matches.size());
	vector<Point2i> matched_scene_keypoints(num_matches);
	vector<Point2i> matched_object_keypoints(num_matches);
	for (int j=0; j<num_matches; j++)
	{	matched_scene_keypoints[j] = scene_keypoints[matches[j].queryIdx].pt;
		matched_object_keypoints[j] = keypoints_[matches[j].trainIdx].pt;
	}
	vector<int> mask(num_matches);
	Mat transform(findHomography(matched_object_keypoints,matched_scene_keypoints,mask,RANSAC)); //find (affine) transform
	int num_inliers(accumulate(mask.begin(),mask.end(),0));
	if (num_inliers<num_matches/2)	//threshold set with trial-and-error
		return false;
	vector<Point2f> left_border({Point2f(0,0),Point2f(0,object_.rows)});	//only segments extrema to pass to cv::line()
	vector<Point2f> right_border({Point2f(object_.cols-1,0),Point2f(object_.cols-1,object_.rows-1)});
	vector<Point2f> up_border({Point2f(0,0),Point2f(object_.cols-1,0)});
	vector<Point2f> down_border({Point2f(0,object_.rows-1),Point2f(object_.cols-1,object_.rows-1)});
	vector<vector<Point2f>> detected_borders(4,vector<Point2f>(2));
	perspectiveTransform(left_border,detected_borders[0],transform);
	perspectiveTransform(down_border,detected_borders[1],transform);
	perspectiveTransform(right_border,detected_borders[2],transform);
	perspectiveTransform(up_border,detected_borders[3],transform);
	for (auto b: detected_borders)
		line(dst,(Point2i)b[0],(Point2i)b[1],Scalar(0,0,255),2.5);
	return true;
};

vector<DMatch> ObjectDetection::FindAccurateMatches(const Mat& query_descriptor,const Mat& train_descriptor,const double& ratio)
{
	vector<DMatch> matches;
	kMatcher->match(query_descriptor, train_descriptor, matches);	//matches input images keypoints' descriptors
	size_t num_matches(matches.size());
	vector<double> distances(num_matches);
	for (int j=0; j<num_matches; j++)
		distances[j] = matches[j].distance;
	double max_distance(ratio**min_element(distances.begin(),distances.end()));	
	for (int j=0; j<matches.size(); j++)
		if (distances[j]>=max_distance)	//discard more inaccurate than ratio matches
		{	matches.erase(matches.begin()+j);
			distances.erase(distances.begin()+j);
			j--;
		}
	return matches;
};
