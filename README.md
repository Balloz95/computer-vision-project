# Computer Vision Project

Final project for my Computer Vision course: given a set of labelled sample images, recover the orientation of an object in a cluttered scene.

In order to make the program work properly, dataset directories
(as "HouseBlack" and "HouseCluttered") should be put one level up the "build" directory.

Ex: 

user/workspace/project$ dir

build include src HouseBlack HouseCluttered

The main program reads datasets with the path "../<dataset_name>" and images with
the path "../<dataset_name>/<image_name>.png".

Link for the dataset repository:

http://www.isy.liu.se/cvl/ImageDB/public/photoboard/2007/
